<?php

Route::get('/', function () {
    return view('index');
});

Route::get('/data-tables', function () {
    return view('data-tables');
});